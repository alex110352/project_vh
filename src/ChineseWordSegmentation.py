import re
from ckiptagger import WS, POS, NER

def clean_mark(article):
    new_line_mark = ':：！？。.，,' #分行用符號
    article = str(article).translate(str.maketrans(dict.fromkeys(new_line_mark, '。'))).split('。')
    for n in range(len(article)):
        keep_chinese = re.compile(r"[^\u4e00-\u9fa5]") #只保留中文
        article[n] = keep_chinese.sub('',article[n])
    article = [sentence for sentence in article if sentence != ''] #刪除空白部分
    return article

def output_word_pos(clean_article,file_name):
    data=open('../docs/'+file_name,'a+',encoding="utf-8")
    ws = WS('../tools/CKIPtagger/')
    pos = POS('../tools/CKIPtagger/')
    word_list = ws(clean_article)   #分單字
    pos_list = pos(word_list)       #分詞性
    for num in range(len(clean_article)):            
        assert len(word_list[num]) == len(pos_list[num])
        for word, pos in zip(word_list[num],pos_list[num]):
            if len(word_list[num]) - 1 != word_list[num].index(word):
                print(word+"["+pos+"]",end=',',file=data)
            else:
                print(word+"["+pos+"]",file=data)
    data.close()
    return 

