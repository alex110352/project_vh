from bs4 import BeautifulSoup
from random import randint
from selenium import webdriver  #需先下載geckodriver至path
import ChineseWordSegmentation as cws
import requests
import time
import re

def yahoo_news_crawler():
    url ='https://tw.news.yahoo.com/politics'
    BasicUrl = "https://tw.news.yahoo.com/"
    urls = []
    news = ''
    response = requests.get(url)
    soup = BeautifulSoup(response.content,'lxml')

    #抓取新聞網址
    for x in range(len(soup.select("h3"))):
        title = soup.select("h3")[x].text.replace('\u3000','')
        num = soup.select("h3")[x].select('a')[0]['href']
        urls.append(BasicUrl+title+num)

    #抓取新聞內容
    for url in urls:
        response = requests.get(url)
        soup = BeautifulSoup(response.content,'lxml')
        for x in range(len(soup.select("p"))):
            news += soup.select("p")[x].text
            news += '\n'
    
    return  news

def novel_crawler(store_file_name):

    #收集書名
    
    titles=[]
    pages = 10      #網頁下滑次數
    suixuan_url = 'https://tw.ttkan.co/novel/class/suixuan'
    driver = webdriver.Firefox()    
    driver.implicitly_wait(3)
    driver.get(suixuan_url)

    for i in range(pages):
        driver.execute_script('window.scrollTo(0, document.body.scrollHeight);')    #網頁下滑指令
        time.sleep(1)
        
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    driver.close()

    for x in range(18*pages):
        titles.append(soup.select('li')[int(3*x)].select('a')[0]['href'].split('/')[-1])
    
    #收集最新級數
    
    for title_num in range(len(titles)):
        novel_url = 'https://tw.ttkan.co/novel/chapters/'+titles[title_num]
        response = requests.get(novel_url)
        soup = BeautifulSoup(response.content,'html.parser')
        try:    
            latest_chapter = soup.select('.show_all_chapters')[0].text
            keep_num = re.compile(r"[^0-9]")    #只保留數字
            latest_chapter = int(keep_num.sub('',latest_chapter))
            latest_chapter = int(soup.select('.near_chapter')[0].select('a')[0]['href'].split('=')[-1])
        except IndexError:
            pass
    
    #收集文章

        for chapter in range(latest_chapter):
            article = []
            novel_chapter_url = 'https://tw.kjasugn.top/novel/pagea/'+titles[title_num]+'_'+str(chapter)+'.html'
            headers = {'User-agent' : 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:22.0) Gecko/20100101 Firefox/22.0'}
            response = requests.get(novel_chapter_url, headers = headers)
            response.encoding='utf-8'
            soup = BeautifulSoup(response.content,'html.parser')
            for x in range(len(soup.select('p'))):
                article.append(soup.select('p')[x].text.replace("\n","").replace(" ","").strip())
            cws.output_word_pos(cws.clean_mark(article),store_file_name+'.csv')
            time.sleep(randint(1,10))

    return 


def ptt_crawler(article_subject,store_file_name):

    #搜尋最新一頁數字
    ptt_title_url = 'https://www.ptt.cc/bbs/'+article_subject+'/index.html'
    headers = {'User-agent' : 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:22.0) Gecko/20100101 Firefox/22.0'}
    response = requests.get(ptt_title_url, headers = headers)
    soup = BeautifulSoup(response.content,'lxml')
    latest_article_num = int(soup.select('.btn.wide')[1]['href'].split('index')[1][:-5])

    #搜尋文章網址
    for article_num in range(latest_article_num):

        article_urls = []
        response = requests.get('https://www.ptt.cc/bbs/'+article_subject+'/index'+str(article_num)+'.html', headers = headers)
        soup = BeautifulSoup(response.content,'lxml')
        for num in range(len(soup.select('.title'))):
            
            #跳過刪除文章
            try:
                article_urls.append('https://www.ptt.cc/'+soup.select('.title')[num].select_one('a')['href'])
            except TypeError:
                pass

        #抓取文章留言
        for url in article_urls:
            article = []
            response = requests.get(url, headers = headers)
            soup = BeautifulSoup(response.content,'lxml')
            for message_num in range(len(soup.select('.f3.push-content'))):
                article.append(soup.select('.f3.push-content')[message_num].text[2:])
        
            #文章輸出到到斷詞程式
            cws.output_word_pos(cws.clean_mark(article),store_file_name+'.csv')
            
        time.sleep(randint(1,10))